# DevOps Technical Check-In Documentation

This repository contains a simple program written in Golang named `pinger`. This service reponds with `"hello world"` at the root path and can be configured to ping another server through the environment variables (see the file at `./cmd/pinger/config.go`). By default, running it will make it start a server and ping itself.

## Pre-requisite

You will need the following installed:

- Go to run the application (check with `go version`)
- Docker for image building/publishing (check with `docker version`)
- Docker Compose for environment provisioning (check with `docker-compose version`)
- Git for source control (check with `git -v`)
- Make for simple convenience scripts (check with `make -v`)

## Get Started

1. Clone this repository
2. Create your own repository on GitLab
3. Set your local repository's remote to point to your GitLab repository
4. Make your changes locally according to the tasks below
5. Push to your GitLab repository


## Need to know

- `Dockerfile` - this file will create an image of our application to be deployed. To keep the image size down we use multistage build. It is composed of two portions, one to build go binary using golang image and we create a slim image using the binary to run the application.
- `docker-compose.yml` - this docker compose file configured to run two containers of same image to ping each other, we configured the docker to use env varibale TARGET_HOST to access other service. We utilise docker default network to access the container using docker container name
- `gitlab-ci.yml` - a file that will be created in the root of the repository and automates dep/test/build/versioning/release process.

**Images:**
- golang:1.13.0-alpine3.10
- alpine:3.7
- golang:1.13.0
- docker:19.03
- mrooding/gitlab-semantic-versioning:latest

## Commands to run the application

1. Build a docker image
   > `make docker_image`
   
2. Test the application
   > `make docker_testrun`
   
3. Run two docker containers   
   > `make testenv`

## CI/CD Pipeline

The pipeline will run automatically whenever you push commit to the repository but you can also run manually in the gitlab UI.

**Stages:**
  - `dep` - download needed dependencies.
  - `test` - run test before build.
  - `build` - build binaries.
  - `generate-env-vars` - generate current tag version
  - `version` - using semver approached. Added script using gitlab-semantic-versioning image to  bump the new version of this repository.
  - `release` - create docker image and saved the created image to tarball.
 
## Versioning

**Need to know:** 

1. Create new branch from the master branch
2. Create merge request and add labels `bump-minor` and/or `bump-major`. These two labels exist to do either a minor or major bump.
3. When you push your commits in the new branch, the pipeline will automatically trigger running only test and gen-env-vars stages. In the merge request, you can choose to automatically merge the new branch to master if the job is successful.
4. If the job is successful, the pipeline will now trigger for master branch running test, gen-env-vars and version stages.
5. The version stage will create new tag and automatically push this to the repository, and will create new pipeline for that new created tag. This tag will now run the dep, test, build, gen-env-vars and release stages.


**Authentication:**

The `NPA_USERNAME` and `NPA_PASSWORD` need to be injected into the version-update container as environment variables. 




